# OpenML dataset: Saugeen-River-Daily

https://www.openml.org/d/46203

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Mean daily flow in cubic meters per second (cumecs) of the Saugeen River.

From original source:
-----
Mean daily flow in cubic meters per second (cumecs) of the Saugeen River at Walkerton, Jan 1,
1915 to Dec 31, 1979
-----

Preprocessing:

1 - Reset index and dropped 'dim_1'.

2 - Renamed 'dim_0' to date and standardize format to %Y-%m-%d.

3 - Created the column 'id_series' with value 0, there is only one long time series.

4 - Ensured that there are no missing dates and that the frequency of the time_series is daily.

5 - Created column 'time_step' with increasing values of time step for the time series.

6 - Casted column 'value_0' to float. Defined 'id_series' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46203) of an [OpenML dataset](https://www.openml.org/d/46203). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46203/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46203/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46203/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

